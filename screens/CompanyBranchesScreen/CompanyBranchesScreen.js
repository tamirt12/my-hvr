import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image, FlatList, 
        Dimensions, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';
import {getDistanceString} from '../../common/distances';
import {compareByName} from '../../common/strings';
import wazeIcon from '../../assets/images/waze.png';
import googleMapsIcon from '../../assets/images/google_maps.png';
import {wazeNavigation, googleMapsNavigation} from '../../api/deep-links';
import constants from '../../common/constants';

class CompanyBranchesScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('companyName'),
      headerStyle: {
        backgroundColor: '#FFEA00'
      }
    };
  };

  constructor(props){
    super(props);
    this.state = {
      filteredBranches: this.props.branches
    };
  }
  
  componentDidUpdate(prevProps, prevState, snapshot){
    if (prevProps.branches !== this.props.branches){
      this.setState({
        filteredBranches: this.props.branches
      });
    }
  }

  /**
   * sort by distance from device if exists, otherwise by name
   */
  getFilteredBranchesOrdered(){
    if (this.props.deviceLocation) {
      return this.state.filteredBranches
        .sort((b1, b2) => b1.distanceFromDeviceInMeters - b2.distanceFromDeviceInMeters)
    }
    else {
      return this.state.filteredBranches.sort((b1, b2) => compareByName(b1.name, b2.name));
    }
  }

  onNavigateWithGoogleMapsPress(branch){
    googleMapsNavigation(branch.latitude, branch.longitude);
  }

  onNavigateWithWazePress(branch){
    wazeNavigation(branch.latitude, branch.longitude);
  }

  searchTimeout = null;
  onSearchTextChange(searchText){
    if (this.searchTimeout){
      clearTimeout(this.searchTimeout);
    }
    searchText = searchText.toLowerCase();
    this.searchTimeout = setTimeout(() => {
        if (!searchText){
          this.setState({
            filteredBranches: this.props.branches
          });
          return;
        }
        const filteredBranches = [];
        for (const branch of this.props.branches){
          if (branch.search(searchText, true)){
            filteredBranches.push(branch);
          }
        }
        this.setState({
          filteredBranches
        });
    }, constants.SEARCH_DELAY_IN_MILLISECONDS)
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput 
              style={styles.searchText}
              placeholder={"חיפוש שם/כתובת סניף"}
              onChangeText={text => this.onSearchTextChange(text)}
          />
          {this.state.filteredBranches.length ===0 && 
            <View style={styles.noResultsContainer}>
              <Text>לא נמצאו תוצאות</Text>
            </View>
          }
          <FlatList data={this.getFilteredBranchesOrdered().map(branch => ({key: branch.id.toString(), branch}))}
            renderItem={({item}) => 
              <View style={styles.branchContainer}>
                <View style={styles.branchDetailsContainer}>
                  <Text style={styles.branchName}>{item.branch.name}</Text>
                  <Text>{item.branch.address}</Text>
                  {this.props.deviceLocation &&
                    <Text style={styles.distanceLabel}>
                      {getDistanceString(item.branch.distanceFromDeviceInMeters)}
                    </Text>
                  }
                </View>
                <View style={styles.branchActionsContainer}>
                  <TouchableOpacity onPress={() => this.onNavigateWithGoogleMapsPress(item.branch)}>
                    <Image source={googleMapsIcon} style={styles.navigationAppImage}/>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.onNavigateWithWazePress(item.branch)}>
                    <Image source={wazeIcon} style={styles.navigationAppImage}/>
                  </TouchableOpacity>
                </View>
              </View>
            }
          />
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fafafa"
  },
  searchText: {
    elevation: 2,
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: 'white',
    marginTop: 10,
    marginBottom: 10
  },
  noResultsContainer: {
    padding: 13,
    borderWidth: 0.3,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  branchContainer: {
    padding: 13,
    borderWidth: 0.3,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  branchDetailsContainer: {
    width: Dimensions.get('window').width - 120,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  branchName: {
    fontWeight: 'bold'
  },
  distanceLabel: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#2E7D32'
  },
  branchActionsContainer: {
    flexDirection: 'row'
  },
  navigationAppImage: {
    height: 30, 
    width: 30, 
    borderRadius: 7,
    marginLeft: 7
  }
});

const mapStateToProps = store => {
  const selectedYellowCompany = store.branches.selectedYellowCompany;
  return {
    branches: selectedYellowCompany ? selectedYellowCompany.branches : [],
    deviceLocation: store.location.deviceLocation
  };
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(CompanyBranchesScreen));
