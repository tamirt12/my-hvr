import React from 'react';
import {StyleSheet, View, FlatList, Text, TouchableNativeFeedback, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {Constants} from 'expo';
import {withNavigation} from 'react-navigation';
import {branchesOperations} from '../../state/branches';
import {appOperations} from '../../state/app';
import {getDistanceString} from '../../common/distances';
import {compareByName} from '../../common/strings';
import constants from '../../common/constants';

class YellowCompaniesScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      filteredCompaniesDict: {}
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if (prevProps.yellowCompaniesDict !== this.props.yellowCompaniesDict){
      this.setState({
        filteredCompaniesDict: this.props.yellowCompaniesDict
      });
    }
  }

  componentDidMount(){
    this.props.setIsLoading(true);
    this.props.fetchYellowCompanies().then(() => {
      this.props.calculateYellowBranchesDistance();
      this.props.setIsLoading(false);
    });
  }

  getFilteredCompanyNamesOrdered(){
    if (this.props.deviceLocation){
      return this.getFilteredCompanyNamesOrderedByDistance();
    }
    else {
      return this.getFilteredCompanyNamesOrderedByName();
    }
  }

  getFilteredCompanyNamesOrderedByDistance(){
    const companies = [];
    for (const companyName in this.state.filteredCompaniesDict)
    {
      companies.push({
        companyName,
        nearestBranchDistance: this.state.filteredCompaniesDict[companyName].nearestBranch ?
          this.state.filteredCompaniesDict[companyName].nearestBranch.distanceFromDeviceInMeters : 
          Number.MAX_SAFE_INTEGER
      });
    }
    return companies.sort((c1, c2) => c1.nearestBranchDistance - c2.nearestBranchDistance)
      .map(company => company.companyName)
  }

  getFilteredCompanyNamesOrderedByName(){
    const companyNames = Object.keys(this.state.filteredCompaniesDict);
    return companyNames.sort((c1, c2) => compareByName(c1, c2));
  }

  searchTimeout = null;
  onSearchTextChange(searchText){
    if (this.searchTimeout){
      clearTimeout(this.searchTimeout);
    }
    searchText = searchText.toLowerCase();
    this.searchTimeout = setTimeout(() => {
        if (!searchText){
          this.setState({
            filteredCompaniesDict: this.props.yellowCompaniesDict
          });
          return;
        }
        const filteredCompaniesDict = {};
        for (const companyName in this.props.yellowCompaniesDict){
          if (companyName.toLocaleLowerCase().includes(searchText)){
            filteredCompaniesDict[companyName] = this.props.yellowCompaniesDict[companyName];
          }
        }
        this.setState({
          filteredCompaniesDict
        });
    }, constants.SEARCH_DELAY_IN_MILLISECONDS)
  }

  navigateToCompanyBranchesScreen(companyName){
    this.props.setSelectedYellowCompany(this.props.yellowCompaniesDict[companyName]);
    this.props.navigation.navigate('CompanyBranches', {companyName});
  }

  onCompanyPress(companyName){
    this.navigateToCompanyBranchesScreen(companyName);
  }

  render() {
    return (
      <View style={styles.container}>
          <TextInput 
              style={styles.searchText}
              placeholder={"חיפוש רשת"}
              onChangeText={text => this.onSearchTextChange(text)}
          />
          {!this.props.isLoading && Object.keys(this.state.filteredCompaniesDict).length ===0 && 
            <View style={styles.noResultsContainer}>
              <Text>לא נמצאו תוצאות</Text>
            </View>
          }
          <FlatList style={styles.companiesContainer}
            data={this.getFilteredCompanyNamesOrdered()
              .map(companyName => ({key: companyName, companyName}))}
            renderItem={({item}) => 
            <TouchableNativeFeedback
              onPress={() => this.onCompanyPress(item.companyName)}
            >
              <View style={styles.companyContainer}>
                <View style={styles.companyLetter}>
                  <Text style={styles.companyLetterText}>{item.companyName ? item.companyName[0] : ''}</Text>
                </View>
                <Text>{item.companyName}</Text>
                {this.props.deviceLocation && this.state.filteredCompaniesDict[item.companyName].nearestBranch &&
                <Text style={styles.distanceLabel}>
                  {getDistanceString(this.state.filteredCompaniesDict[item.companyName].nearestBranch.distanceFromDeviceInMeters)}
                </Text>
                }
              </View>
            </TouchableNativeFeedback>
            }
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight 
  },
  companiesContainer: {
    backgroundColor: 'white'
  },
  companyContainer: {
    padding: 13,
    borderWidth: 0.3,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  companyLetter: {
    backgroundColor: '#303030',
    borderWidth: 0.7,
    borderColor: '#bdbdbd',
    borderRadius: 50,
    marginRight: 15,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30
  },
  companyLetterText: {
    color: '#FFFF8D',
    fontWeight: 'bold',
    fontSize: 14
  },
  distanceLabel: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#2E7D32',
    marginLeft: 'auto'
  },
  searchText: {
    elevation: 2,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    backgroundColor: 'white',
    margin: 8
  },
  noResultsContainer: {
    padding: 13,
    borderWidth: 0.3,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});

const mapStateToProps = (store) => {
  return {
    yellowCompaniesDict: store.branches.yellowCompaniesDict,
    deviceLocation: store.location.deviceLocation,
    isLoading: store.app.isLoading
  }
}

const mapDispatchToProps = {
  setIsLoading: appOperations.setIsLoading,
  fetchYellowCompanies: branchesOperations.fetchYellowCompanies,
  calculateYellowBranchesDistance: branchesOperations.calculateYellowBranchesDistance,
  setSelectedYellowCompany: branchesOperations.setSelectedYellowCompany
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(YellowCompaniesScreen));
