import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View} from 'react-native';
import {withNavigation} from 'react-navigation';
import {branchesOperations} from '../../state/branches';
import {locationOperations} from '../../state/location';
import BranchesView from '../../components/BranchesView/BranchesView';

class BlueBranchesScreen extends React.Component {

  constructor(props){
    super(props);
    this.props.fetchBlueBranches().then(() => {
      this.initLocation();
    });
  }

  initLocation(){
    navigator.geolocation.getCurrentPosition(position => {
      const deviceLocation = {
        latitude: position.coords.latitude, 
        longitude: position.coords.longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      };
      this.props.setDeviceLocation(deviceLocation);
      this.props.setIsLocationInitialized(true);
      this.props.calculateBlueBranchesDistance();
    }, error => {
      // location services aren't avaiable, calculate distance from default location
      this.props.setIsLocationInitialized(true);
      this.props.calculateBlueBranchesDistance();
    });
  }

  render() {
    return (
      <View style={styles.container}>
          <BranchesView 
            branches={this.props.blueBranches}
            shouldShowMap={true}
            searchPlaceholder='חיפוש מסעדה/עיר'
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = (store) => {
  return {
    blueBranches: store.branches.blueBranches
  }
}

const mapDispatchToProps = {
  fetchBlueBranches: branchesOperations.fetchBlueBranches,
  calculateBlueBranchesDistance: branchesOperations.calculateBlueBranchesDistance,
  setDeviceLocation: locationOperations.setDeviceLocation,
  setIsLocationInitialized: locationOperations.setIsLocationInitialized
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(BlueBranchesScreen));
