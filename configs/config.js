
export default {
    blueBranchesURL: 'https://www.hvr.co.il/static/foodcard_branches.json',
    yellowBranchesURL: 'https://www.hvr.co.il/static/giftcard_branches.json'
};