
import geolib from 'geolib';
import api from '../../api/api';
import actions from './actions';
import Branch from '../../models/branch';

const fetchBlueBranches = () => {
    return (dispatch) => {
        return api.getBlueBranches()
        .then(blueBranches => {
            dispatch(actions.receiveBlueBranches(blueBranches));
            return blueBranches;
        })
        .catch(err => {
            throw err;
        });
    }
};

const fetchYellowCompanies = () => {
    return (dispatch) => {
        return api.getYellowCompaniesDict()
        .then(yellowCompaniesDict => {
            dispatch(actions.receiveYellowCompaniesDict(yellowCompaniesDict));
            return yellowCompaniesDict;
        })
        .catch(err => {
            throw err;
        });
    }
};

const calculateBlueBranchesDistance = () => {
    return (dispatch, getState) => {
        const location = getState().location.deviceLocation ||
            getState().location.defaultLocation; // if device location isn't available, use default one
        const previousBlueBranches = getState().branches.blueBranches;
        const updatedBlueBranches = _calculateBranchesDistance(location, previousBlueBranches);
        dispatch(actions.receiveBlueBranches(updatedBlueBranches));
    }
}

const calculateYellowBranchesDistance = () => {
    return (dispatch, getState) => {
        const location = getState().location.deviceLocation ||
            getState().location.defaultLocation; // if device location isn't available, use default one
        const yellowCompaniesDict = getState().branches.yellowCompaniesDict;
        for (const companyName in yellowCompaniesDict){
            const updatedBranches = _calculateBranchesDistance(location, 
                yellowCompaniesDict[companyName].branches);
            yellowCompaniesDict[companyName].branches = updatedBranches;
            yellowCompaniesDict[companyName].nearestBranch = 
                Branch.getNearestBranch(updatedBranches);
        }
        dispatch(actions.receiveYellowCompaniesDict(yellowCompaniesDict));
    }
}

const setSelectedYellowCompany = company => {
    return dispatch => {
        dispatch(actions.setSelectedYellowCompany(company));
    };
}

const _calculateBranchesDistance = (location, branches) => {
    const updatedBranches = [];
    for (const branch of branches){
        updatedBranches.push(new Branch({
            ...branch,
            distanceFromDeviceInMeters: geolib.getDistance(
                {
                latitude: location.latitude,
                longitude: location.longitude
                },
                {
                latitude: branch.latitude,
                longitude: branch.longitude
                }
            )
        }));
    }
    return updatedBranches;
}

export default {
    fetchBlueBranches,
    fetchYellowCompanies,
    calculateBlueBranchesDistance,
    calculateYellowBranchesDistance,
    setSelectedYellowCompany
}