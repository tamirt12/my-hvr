import types from "./types";

const initalState = {
    blueBranches: [],
    yellowCompaniesDict: {},
    selectedYellowCompany: null
};

export default (state = initalState, action) => {
    switch (action.type){
        case types.RECEIVE_BLUE_BRANCHES:
            return {
                ...state,
                blueBranches: [...action.branches]
            };
        case types.RECEIVE_YELLOW_COMPANIES_DICT:
            return {
                ...state,
                yellowCompaniesDict: {...action.dict}
            };
        case types.SET_SELECTED_YELLOW_COMPANY:
            return {
                ...state,
                selectedYellowCompany: action.company
            };
        default:
            return state;
    }
}