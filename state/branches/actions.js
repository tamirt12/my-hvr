
import types from './types';

const receiveBlueBranches = branches => {
    return {
        type: types.RECEIVE_BLUE_BRANCHES,
        branches
    };
};

const receiveYellowCompaniesDict = dict => {
    return {
        type: types.RECEIVE_YELLOW_COMPANIES_DICT,
        dict
    };
}

const setSelectedYellowCompany = company => {
    return {
        type: types.SET_SELECTED_YELLOW_COMPANY,
        company
    };
}

export default {
    receiveBlueBranches,
    receiveYellowCompaniesDict,
    setSelectedYellowCompany
}