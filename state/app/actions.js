
import types from './types';

const setIsLoading = isLoading => {
    return {
        type: types.SET_IS_LOADING,
        isLoading
    };
};

export default {
    setIsLoading
}