import types from "./types";

const initalState = {
    isLoading: false
};

export default (state = initalState, action) => {
    switch (action.type){
        case types.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        default:
            return state;
    }
}