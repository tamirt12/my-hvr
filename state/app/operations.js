
import actions from './actions';

const setIsLoading = isLoading => {
    return (dispatch) => {
        dispatch(actions.setIsLoading(isLoading));
    }
};

export default {
    setIsLoading
}