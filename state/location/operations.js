
import actions from './actions';

const setDeviceLocation = ({latitude, longitude}) => {
    return dispatch => {
        dispatch(actions.setDeviceLocation({latitude, longitude}));
    }
};

const setIsLocationInitialized = isInitialized => {
    return dispatch => {
        dispatch(actions.setIsLocationIntialized(isInitialized));
    }
}

export default {
    setDeviceLocation,
    setIsLocationInitialized
}