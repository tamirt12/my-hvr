
import types from './types';

const initalState = {
    defaultLocation: {
        latitude: 32.0863536,
        longitude: 34.7901598,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
    },
    deviceLocation: null,

    // this becomes true when we've completed trying to acquire the device's location
    isLocationInitialized: false 
}

export default (state = initalState, action) => {
    switch (action.type){
        case types.SET_DEVICE_LOCATION:
            return {
                ...state,
                deviceLocation: {
                    latitude: action.latitude,
                    longitude: action.longitude,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01
                }
            };
        case types.SET_IS_LOCATION_INITIALIZED:
            return {
                ...state,
                isLocationInitialized: action.isInitialized
            }
        default:
            return state;
    }
}