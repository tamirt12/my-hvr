
import types from './types';

const setDeviceLocation = ({latitude, longitude}) => {
    return {
        type: types.SET_DEVICE_LOCATION,
        latitude,
        longitude
    };
};

const setIsLocationIntialized = isInitialized => {
    return {
        type: types.SET_IS_LOCATION_INITIALIZED,
        isInitialized
    }
}

export default {
    setDeviceLocation,
    setIsLocationIntialized
}