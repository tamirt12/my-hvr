
import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from './reducers';

const rootReducer = combineReducers(reducers);
export default function configureStore() {
    return createStore(
        rootReducer,
        applyMiddleware(thunk)
    );
}