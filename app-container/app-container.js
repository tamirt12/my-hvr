import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, ActivityIndicator, Dimensions} from 'react-native';
import AppNavigator from '../navigation/AppNavigator';

class AppContainer extends React.Component {
  render() {
    return (
        <View style={styles.container}>
            <AppNavigator/>
            {this.props.isLoading && 
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
            }
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    loadingContainer: {
        position: 'absolute',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        backgroundColor: '#bdbdbd',
        opacity: 0.7,
        justifyContent: 'center',
        alignItems: 'center'
    },
});

const mapStateToProps = (store) => {
  return {
    isLoading: store.app.isLoading
  }
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
