import React from 'react';
import {StyleSheet, View, TextInput, ScrollView, Dimensions, 
        KeyboardAvoidingView, Keyboard, TouchableNativeFeedback, ActivityIndicator,
        Text} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import constants from '../../common/constants';

class AutoSuggest extends React.PureComponent {

    constructor(props){
        super(props);
        this.state = {
            searchText: '',
            keyboardHeight: 0,
            isSearching: false,
            searchTimeout: null
        };
    }

    componentWillMount(){
        Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow(e) {
        this.setState({keyboardHeight: e.endCoordinates.height});
    }

    _keyboardDidHide(e){
        this.setState({keyboardHeight: 0});
    }

    onSearchTextChange(searchText){
        if (!searchText){
            this.setState({
                searchText
            });
            this.props.setSuggestedItems([]);
            return;
        }
        this.setState({
            searchText,
            isSearching: true
        }, () => {
            if (this.state.searchTimeout){
                clearTimeout(this.state.searchTimeout);
            }
            this.state.searchTimeout = setTimeout(() => {
                const items = this.props.items || [];
                let suggestedItems = items.filter(item => this.props.shouldSuggestItem(this.state.searchText, item));
                if (this.props.compareItems){
                    suggestedItems = suggestedItems.sort(this.props.compareItems);
                }
                this.setState({
                    isSearching: false
                });
                this.props.setSuggestedItems(suggestedItems);
            }, constants.SEARCH_DELAY_IN_MILLISECONDS)
        });
    }

    /** 
     * getting the style from a method since its dependant on the component's state 
     * (whether keyboard is open or not)
    */
    getSuggestionsContainerStyle(){
        return {
            marginTop: 5,
            backgroundColor: 'white',
            padding: 8,
            borderColor: '#e0e0e0',
            borderWidth: 0.7,
            borderRadius: 4,
            maxHeight: Dimensions.get('window').height - this.state.keyboardHeight - 200
          };
    }

    onItemPress(item){
        this.onSearchTextChange('');
        this.props.onItemPress(item);
    }

    onTextInputFocus(){
        this.props.onFocus();
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.autoSuggestContainer}>
                <View>
                    <TextInput 
                        style={styles.searchText}
                        value={this.state.searchText}
                        placeholder={this.props.placeholder}
                        onChangeText={text => this.onSearchTextChange(text)}
                        onFocus={() => this.onTextInputFocus()}
                    />
                    {(this.state.isSearching || 
                    (this.props.suggestedItems && this.props.suggestedItems.length > 0)) && 
                    <View style={this.getSuggestionsContainerStyle()}>
                        <ScrollView>
                            {this.state.isSearching && <ActivityIndicator size="small"/>}
                            {this.props.suggestedItems.map((item, i) => 
                            <TouchableNativeFeedback key={i}
                                onPress={() => this.onItemPress(item)}
                            >
                                <View>
                                    {this.props.renderItem(item)}
                                </View>
                            </TouchableNativeFeedback>
                            )}
                        </ScrollView>
                    </View>
                    }
                    {this.props.shouldShowPlaceholder &&!this.state.isSearching && 
                    (!this.props.suggestedItems || this.props.suggestedItems.length == 0) && 
                    <View style={styles.placeholderContainer}>
                        <MaterialIcons name="search" size={80}/>
                        {this.state.searchText ? 
                            <Text>אין תוצאות</Text>
                            :
                            <Text>יש להזין חיפוש</Text>
                        }
                    </View>
                    }
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    autoSuggestContainer: {
    },
    searchText: {
        elevation: 2,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: 'white'
    },
    placeholderContainer: {
        alignItems: 'center',
        marginTop: 140,
        opacity: 0.7
    }
});

export default AutoSuggest;

