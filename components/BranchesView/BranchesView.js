import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View, TouchableOpacity, Text, Keyboard, 
        Dimensions, BackHandler, Image} from 'react-native';
import {withNavigation} from 'react-navigation';
import MapView, {Marker} from 'react-native-maps';
import {MaterialIcons, Entypo} from '@expo/vector-icons';
import BranchMarker from '../BranchMarker';
import AutoSuggest from '../../components/AutoSuggest/AutoSuggest';
import {getDistanceString} from '../../common/distances';
import Branch from '../../models/branch';
import {appOperations} from '../../state/app';
import wazeIcon from '../../assets/images/waze.png';
import googleMapsIcon from '../../assets/images/google_maps.png';
import { wazeNavigation, googleMapsNavigation } from '../../api/deep-links';

class BranchesView extends React.Component {
    
  constructor(props){
    super(props);
    this.state = {
      isMapReady: false,
      selectedBranch: null,
      suggestedBranches: []
    }
  }

  componentDidMount(){
    this.props.setIsLoading(true);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentDidUpdate(prevProps, prevState, snapshot){
      if (prevProps.isLoading){
        if (this.props.isLocationInitialized){
          if (!this.props.shouldShowMap || this.state.isMapReady){
            this.props.setIsLoading(false);
          }
        }
      }
      if (prevProps.deviceLocation !== this.props.deviceLocation){
          // if device location changed, navigate to the updated one
          this.goToLocation(this.props.deviceLocation);
      }
  }

  onMapReady(){
    this.setState({
      isMapReady: true
    })
  }

  handleBackPress = () => {
    if (!this.state.selectedBranch && 
      (!this.state.suggestedBranches || this.state.suggestedBranches.length === 0)){
      return false;
    }
    this.setState({
      selectedBranch: null,
      suggestedBranches: []
    });
    return true;
  }

  onGoToDeviceLocationPress(){
    if (this.props.deviceLocation){
      this.goToLocation(this.props.deviceLocation);
    }
    else {
      this.goToLocation(this.props.defaultLocation);
    }
    this.setState({
      selectedBranch: null
    });
  }

  /**
   * @param location {latitude, longitude, latitudeDelta, longitudeDelta}
   */
  goToLocation(location){
    if (this.mapViewRef){
      this.mapViewRef.animateToRegion(location);
    }
  }

  onBranchSuggestionPress(branch){
    this.goToLocation({
      latitude: branch.latitude,
      longitude: branch.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    });
    this.setState({
      selectedBranch: branch
    });
  }

  onMapPress(){
    Keyboard.dismiss();
    this.setState({
      selectedBranch: null
    })
  }

  onMarkerPress(branch){
    Keyboard.dismiss();
    this.setState({
      selectedBranch: branch,
      suggestedBranches: []
    });
  }

  onScreenPress(){
    console.log('screen press');
  }

  onAutoSuggestFocus(){
    this.setState({
      selectedBranch: null
    });
  }

  onSetSuggestedBranches(branches){
    this.setState({
      suggestedBranches: branches
    });
  }

  onNavigateWithWazePress(){
    wazeNavigation(this.state.selectedBranch.latitude, this.state.selectedBranch.longitude);
  }

  onNavigateWithGoogleMapsPress(){
    googleMapsNavigation(this.state.selectedBranch.latitude, this.state.selectedBranch.longitude);
  }

  getDeviceLocationButtonStyle(){
    const bottom = this.state.selectedBranch ? 160 : 70;
    return {
      position: 'absolute',
      bottom,
      right: 15,
      backgroundColor: 'white', 
      borderWidth: 1,
      borderRadius: 30,
      borderColor: "#E0E0E0",
      width: 50, 
      height: 50,
      alignItems: 'center',
      justifyContent: 'center'
    };
  }

  renderBranchSuggestion(branch){
    return (
        <View style={styles.branchSuggestionContainer}>
          <View style={{
            flexDirection: 'row', 
            alignItems: 'center', 
            justifyContent: 'space-between'
          }}>
            <Text numberOfLines={1} style={{
              fontWeight: 'bold',
              maxWidth: 200,
              marginRight: 10,
              color: branch.type === Branch.BranchType.BLUE ? "#03A9F4" : "black"
            }}>
              {branch.name}
            </Text>
            {this.props.deviceLocation && 
              <Text>{getDistanceString(branch.distanceFromDeviceInMeters)}</Text>
            }
          </View>
          <Text numberOfLines={1}>{branch.getFullAddress()}</Text>
        </View>
    );
  }

  renderSelectedBranchCallout(){
    return (
      this.state.selectedBranch ? 
      <View style={styles.branchCallout}>
        <Text style={styles.branchCalloutHeader} numberOfLines={1}>{this.state.selectedBranch.name}</Text>
        <Text numberOfLines={1}>{this.state.selectedBranch.description}</Text>
        <View style={styles.branchCalloutLocationContainer}>
          <Entypo name="location-pin" size={20}/>
          <Text numberOfLines={1}>{this.state.selectedBranch.getFullAddress()}</Text>
        </View>
        <View style={styles.branchCalloutNavigationContainer}>
          <TouchableOpacity onPress={() => this.onNavigateWithGoogleMapsPress()}>
            <Image source={googleMapsIcon} style={styles.navigationAppImage}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.onNavigateWithWazePress()}>
            <Image source={wazeIcon} style={styles.navigationAppImage}/>
          </TouchableOpacity>
        </View>
      </View>
      :
      null
    );
  }

  render() {
    return (
      <View style={styles.container} onPress={() => this.onScreenPress()}>
          {this.props.shouldShowMap && 
          <MapView
            style={styles.map}
            initialRegion={this.props.defaultLocation}
            ref={mapViewRef => this.mapViewRef = mapViewRef}
            onMapReady={() => this.onMapReady()}
            onPress={() => this.onMapPress()}
          >
            {this.props.branches.map((branch, i) => 
              !!branch.latitude && !!branch.longitude && 
              <BranchMarker
                key={i}
                branch={branch}
                isSelected={this.state.selectedBranch ? 
                  this.state.selectedBranch.id === branch.id : false}
                onMarkerPress={branch => this.onMarkerPress(branch)}
              />
            )}
            {this.props.deviceLocation && 
              <Marker
                title="מיקום נוכחי"
                coordinate={{
                  latitude: this.props.deviceLocation.latitude,
                  longitude: this.props.deviceLocation.longitude
                }}
              >
                <View style={styles.deviceLocationView}/>
              </Marker>
            }
          </MapView>
          }
          {this.props.shouldShowMap && 
          <TouchableOpacity style={this.getDeviceLocationButtonStyle()}
            onPress={() => this.onGoToDeviceLocationPress()}
          >
              <MaterialIcons name="my-location" size={24}/>
          </TouchableOpacity>
          }
          <View style={styles.autoSuggestContainer}>
            <AutoSuggest
              placeholder={this.props.searchPlaceholder}
              items={this.props.branches}
              suggestedItems={this.state.suggestedBranches}
              shouldShowPlaceholder={!this.props.shouldShowMap}
              renderItem={branch => this.renderBranchSuggestion(branch)}
              shouldSuggestItem={(searchText, branch) => branch.search(searchText)}
              compareItems={(branch1, branch2) => Branch.compareByDistance(branch1, branch2)}
              onItemPress={branch => this.onBranchSuggestionPress(branch)}
              onFocus={() => this.onAutoSuggestFocus()}
              setSuggestedItems={branches => this.onSetSuggestedBranches(branches)}
            />
          </View>
          {this.renderSelectedBranchCallout()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1
  },
  deviceLocationView: {
    backgroundColor: "#455A64", 
    borderWidth: 2,
    borderRadius: 15,
    borderColor: "#81D4FA",
    width: 20, 
    height: 20
  },
  branchSuggestionContainer: {
    paddingTop: 8,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#E0E0E0'
  },
  branchSuggestionName: {
    fontWeight: 'bold',
    maxWidth: 200,
    marginRight: 10
  },
  autoSuggestContainer: {
    width: Dimensions.get('window').width - 20,
    position: 'absolute',
    top: 40,
    marginHorizontal: 10
  },
  branchCallout: {
    position: 'absolute',
    bottom: 15,
    left: 5,
    width: Dimensions.get('window').width - 10,
    backgroundColor: 'white',
    elevation: 3,
    padding: 13,
    borderRadius: 10,
    height: 140,
  },
  branchCalloutHeader: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  branchCalloutLocationContainer: {
    flexDirection: 'row', 
    opacity: 0.6, 
    marginTop: 15, 
    marginLeft: -5
  },
  branchCalloutNavigationContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  navigationAppImage: {
    height: 40, 
    width: 40, 
    borderRadius: 7,
    marginLeft: 4
  }
});

const mapStateToProps = (store) => {
  return {
    isLoading: store.app.isLoading,
    defaultLocation: store.location.defaultLocation,
    deviceLocation: store.location.deviceLocation,
    isLocationInitialized: store.location.isLocationInitialized
  }
}

const mapDispatchToProps = {
  setIsLoading: appOperations.setIsLoading
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(BranchesView));
