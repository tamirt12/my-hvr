import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {Marker} from 'react-native-maps';
import Branch from '../models/branch';

class BranchMarker extends React.PureComponent {

  onMarkerPress(){
    this.props.onMarkerPress(this.props.branch);
  }

  render() {
    return (
      <Marker
        key={this.props.isSelected ? 1 : 2} // workaround to get the marker's color to change once its selected
        coordinate={{
          latitude: this.props.branch.latitude,
          longitude: this.props.branch.longitude
        }}
        pinColor={this.props.isSelected ? "#E53935" :
                  this.props.branch.type === Branch.BranchType.BLUE ? "#03A9F4" : 
                  this.props.branch.type === Branch.BranchType.YELLOW ? "#FFFF00" : "black"}
        onPress={() => this.onMarkerPress()}
      />
    );
  }
}

const styles = StyleSheet.create({
  callout: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontWeight: 'bold'
  },
  locationContainer: {
    borderTopWidth: 1,
    borderTopColor: '#bdbdbd',
    marginTop: 6,
    flexDirection: 'column',
    alignItems: 'center',
    padding: 4
  }
});

const mapStateToProps = (store) => {
  return {
    deviceLocation: store.location.deviceLocation,
  }
}

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(BranchMarker);

