
// TODO get branches from db

branchSequentialId = 1;
export default class Branch {
    constructor({name, description, type, latitude, longitude, city, address, distanceFromDeviceInMeters, phone}){
        this.id = branchSequentialId++;
        this.name = name;
        this.description = description;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.address = address;
        this.distanceFromDeviceInMeters = distanceFromDeviceInMeters;
        this.phone = phone;
    }

    search(searchText, includeAddress = false){
        if (!searchText || !searchText.toLowerCase){
            return false;
        }
        searchText = searchText.toLowerCase();
        return ((this.name && this.name.toLowerCase().includes(searchText)) ||
               (this.city && this.city.toLowerCase().includes(searchText)) ||
               (includeAddress && this.address && this.address.toLowerCase().includes(searchText)))
    }

    getFullAddress(){
        return this.address && this.city ? 
            `${this.address}, ${this.city}` 
            :
            this.address ? this.address : this.city
    }

    static compareByDistance(branch1, branch2){
        if (!branch1 && !branch2){
            return 0;
        }
        if (!branch1 || branch2 === 0){
            return 1;
        }
        if (!branch2 || branch1 === 0){
            return -1;
        }
        const distance1 = branch1.distanceFromDeviceInMeters || Number.MAX_SAFE_INTEGER;
        const distance2 = branch2.distanceFromDeviceInMeters || Number.MAX_SAFE_INTEGER;

        return distance1 - distance2;
    }

    static getNearestBranch(branches)
    {
        let nearestDistance = Number.MAX_SAFE_INTEGER;
        let nearestBranch = null;
        for (const branch of branches){
            if (branch.distanceFromDeviceInMeters < nearestDistance)
            {
                nearestBranch = branch;
                nearestDistance = branch.distanceFromDeviceInMeters;
            }
        }
        return nearestBranch;
    }

    static BranchType = {
        BLUE: 'blue',
        YELLOW: 'yellow'
    }
}