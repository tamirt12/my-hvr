
export default class YellowCompany{
    constructor({companyName, branches}){
        this.companyName = companyName;
        this.branches = branches;
        this.nearestBranch = null;
    }
}