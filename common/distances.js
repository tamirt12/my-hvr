
function getDistanceString(distanceInMeters){
    if (!distanceInMeters || !(+distanceInMeters)){
        return '';
    }
    if (distanceInMeters < 1000){
        return `${distanceInMeters} מ'`;
    }
    return `${(distanceInMeters/1000).toFixed(2)} ק"מ`;
}

export {
    getDistanceString
}