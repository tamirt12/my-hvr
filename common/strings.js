
function containsHebrew(str){
    return str.search(/[\u0590-\u05FF]/);
}

function compareByName(name1, name2){
    if (!name1 || name1.length === 0){
        return -1;
    }
    if (!name2 || name2.length === 0){
        return 1;
    }

    [name1ContainsHebrew, name2ContainsHebrew] = 
        [containsHebrew(name1), containsHebrew(name2)];
    if (name1ContainsHebrew && !name2ContainsHebrew){
        return 1;
    }
    if (name2ContainsHebrew && !name1ContainsHebrew){
        return -1;
    }

    return name1.charCodeAt(0) - name2.charCodeAt(0);
}

export {
    containsHebrew,
    compareByName
}