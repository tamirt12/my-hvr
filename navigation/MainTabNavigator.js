import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import BlueBranchesScreen from '../screens/BlueBranchesScreen/BlueBranchesScreen';
import YellowCompaniesScreen from '../screens/YellowCompaniesScreen/YellowCompaniesScreen';
import CompanyBranchesScreen from '../screens/CompanyBranchesScreen/CompanyBranchesScreen';

const blueBranchesStack = createStackNavigator(
  {
    Home: BlueBranchesScreen
  }, 
  {
    headerMode: 'none'
  }
);

blueBranchesStack.navigationOptions = {
  tabBarLabel: 'מסעדות - חבר כחול',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-restaurant${focused ? '' : '-outline'}`
          : 'md-restaurant'
      }
    />
  ),
};

const yellowBranchesStack = createStackNavigator(
  {
    Home: {
      screen: YellowCompaniesScreen,
      navigationOptions: {
        header: null,
      }
    },
    CompanyBranches: CompanyBranchesScreen
  }, 
  {
    cardStyle: {
      backgroundColor: '#eeeeee'
    }
  }
);

yellowBranchesStack.navigationOptions = {
  tabBarLabel: 'חנויות - חבר צהוב',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-cart${focused ? '' : '-outline'}`
          : 'md-cart'
      }
    />
  ),
};

export default createBottomTabNavigator({
  blueBranchesStack,
  yellowBranchesStack
});