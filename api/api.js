import Branch from "../models/branch";
import config from "../configs/config";
import YellowCompany from "../models/yellow-company";

async function getBlueBranches(){
    const response = await fetch(config.blueBranchesURL);
    return (await response.json()).branch.map(branch => new Branch({
        name: branch.name,
        description: branch.desc,
        type: Branch.BranchType.BLUE,
        latitude: +branch.latitude,
        longitude: +branch.longitude,
        city: branch.city,
        address: branch.address
    }));
}

async function getYellowCompaniesDict(){
    const response = await fetch(config.yellowBranchesURL);
    const companyNameToBranchesDict = await response.json();
    const yellowCompaniesDict = {};
    for (const companyName in companyNameToBranchesDict){
        const branches = companyNameToBranchesDict[companyName].map(branch => new Branch({
            name: `${branch.name}`,
            company: companyName,
            type: Branch.BranchType.YELLOW,
            latitude: +branch.latitude,
            longitude: +branch.longitude,
            address: branch.address,
            phone: branch.phone
        }));
        yellowCompaniesDict[companyName] = new YellowCompany({
            companyName,
            branches
        });
    }
    return yellowCompaniesDict;
}

export default {
    getBlueBranches,
    getYellowCompaniesDict
}