
import AppLink from 'react-native-app-link';

function wazeNavigation(latitude, longitude){
    const url = `https://waze.com/ul?ll=${latitude},${longitude}&z=10`;
    AppLink.maybeOpenURL(url, {
      appName: 'Waze'
    }).then(() => {

    })
    .catch(err => {
      alert('התרחשה שגיאה')
    });
  }

function googleMapsNavigation(latitude, longitude){
    const url = `https://www.google.com/maps/dir/?api=1&destination=${latitude},${longitude}`;
    AppLink.maybeOpenURL(url, {
        appName: 'Google Maps'
    }).then(() => {

    })
    .catch(err => {
        alert('התרחשה שגיאה')
    });
  }

  export {
      wazeNavigation,
      googleMapsNavigation
  }